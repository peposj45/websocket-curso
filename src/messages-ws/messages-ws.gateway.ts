import { OnGatewayConnection, OnGatewayDisconnect, WebSocketGateway } from '@nestjs/websockets';
import { MessagesWsService } from './messages-ws.service';

import { Socket } from 'socket.io';

@WebSocketGateway(88, { cors: true })
export class MessagesWsGateway implements OnGatewayConnection, OnGatewayDisconnect {

  constructor(
    private readonly messagesWsService: MessagesWsService
  ) {}

  handleConnection(client: Socket ) {
    console.log(`Cliente conectado ${client.id}`);
  }
  
  handleDisconnect(client: Socket) {
    console.log(`Cliente desconectado ${client.id}`);
  }

}
