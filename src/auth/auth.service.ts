import { BadRequestException, Injectable, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';

import * as bcrypt from "bcrypt";

import { User } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { JwtPayload } from './interfaces/jwt-payload.interface';

@Injectable()
export class AuthService {

  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService
  ){}

  async create(createUserDto: CreateUserDto) {
    try {
      const { password, ...userData } = createUserDto;
      const user = this.userRepository.create({
        ...userData,
        password: bcrypt.hashSync(password, 12)
      });

      await this.userRepository.save(user);
      delete user.password;
      return user;
    } catch (error) {
      console.log(error);
      this.handelDBErrors(error);
    }
  }

  async login(loginUserDto: LoginUserDto){
    const { password, email } = loginUserDto;
    const user = await this.userRepository.findOne({
      where:{email},
      select: { id: true, fullName: true, email: true, password: true, isActive: true }
    });

    if( !user )
      throw new UnauthorizedException('Credentials are not valid (email)');

    if( !user.isActive )
      throw new UnauthorizedException('Credentials are not valid (no activo)');

    if(!bcrypt.compareSync(password,user.password ))
      throw new UnauthorizedException('Credentials are not valid (password)');

    return {
      ...user,
      token: this.getJwtToekn({id: user.id})
    };
  }
  checkAuthStatus( user: User ){
    return {
      ...user,
      token: this.getJwtToekn({id: user.id})
    };
  }

  private getJwtToekn( payload: JwtPayload ){
    const token = this.jwtService.sign(payload);
    return token;
  }

  private handelDBErrors(error: any): never{
    if(error.code === '23505')
      throw new BadRequestException(error.detail);
    console.log(error);
    throw new InternalServerErrorException('Please check server logs');
  }

}