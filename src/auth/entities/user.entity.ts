import { Column, Entity, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, OneToMany } from 'typeorm';

import { Product } from "../../products/entities";

@Entity('users')
export class User {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text',{
        unique: true
    })
    email: string;

    @Column('text',{
        select: false
    })
    password: string;

    @Column('text',{

    })
    fullName: string;

    @Column('bool',{
        default: 1
    })
    isActive: boolean;

    @Column('text',{
        array: true,
        default: ['user']
    })
    roles: string [];

    @OneToMany(
        () => Product,
        ( product ) => product.user
    )
    product: Product

    @BeforeInsert()
    checkFliedsInsert(){
        this.email = this.email.toLocaleLowerCase().trim();
    }
    
    @BeforeUpdate()
    checFieldsUpdate(){
        this.email = this.email.toLocaleLowerCase().trim();
    }
}