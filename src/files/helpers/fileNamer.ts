import { Request } from "express";
import { v4 as uuid } from "uuid";

export const fileNamer = (req: Request, file: Express.Multer.File, calback: Function) => {
    // console.log({req});

    // if( !file ) return calback( new Error('File is empty'), false );
    const fileExtension = file.mimetype.split('/')[1];
    const fileName      = `${uuid()}.${fileExtension}`;
    
    calback(null, fileName);
}